import 'dart:typed_data';
import 'package:durt/durt.dart';

Future<void> main() async {
  const String mnemonicLang = 'french';

  print('------------------\n HD Wallet example\n------------------\n');
  var mnemonic = generateMnemonic(lang: mnemonicLang);

  // Build HdWallet object from mnemonic
  HdWallet _hdWallet = HdWallet.fromMnemonic(mnemonic);

  // Get pubkey of derivation 0
  String pubkey0 = _hdWallet.getPubkey(0);

  String message = "blabla test";

  // Sign the message with derivation 0
  var signature = _hdWallet.sign(message, derivation: 0);
  bool isOK = _hdWallet.verifySign(message, signature, derivation: 0);

  print('Mnemonic: ' + mnemonic);
  print('Pubkey 0: ' + pubkey0);
  print('Is signature OK ? : ' + isOK.toString());

  print('\n------------------\n DEWIF example\n------------------\n');

  var _dewif = Dewif();

  var _dewifData =
      await _dewif.generateDewif(mnemonic, 'ABCDE', lang: mnemonicLang);
  print(_dewifData);

  String? decryptedDewif;
  try {
    decryptedDewif =
        _dewif.mnemonicFromDewif(_dewifData.dewif, 'ABCDE', lang: mnemonicLang);
    print('Unlock: ' + decryptedDewif);
  } on ChecksumException {
    print('Bad secret code');
  } catch (e) {
    print(e);
  }

  print(
      '\n------------------\n Cesium wallet generation example\n------------------\n');

  // Build Cesium wallet from salt + password
  var cesiumWallet =
      CesiumWallet('myCesiumID', 'myCesiumPassword'); // Cesium ID + Password

  print('Seed: ' + cesiumWallet.seed.toString());
  print('Pubkey: ' + cesiumWallet.pubkey);

  var signatureCesium = cesiumWallet.sign(message);
  bool isOKCesium = cesiumWallet.verifySign(message, signatureCesium);

  // Is signature valid ?
  print(isOKCesium); //true

  print('\n------------------\n Dewif for Cesium wallet\n------------------\n');

  final _dewifCesiumData =
      await _dewif.generateCesiumDewif(cesiumWallet.seed, 'FGHIJ');

  Uint8List? decryptedCesiumDewif;
  try {
    decryptedCesiumDewif =
        _dewif.cesiumSeedFromDewif(_dewifCesiumData.dewif, 'FGHIJ');
    print('Unlock: ' + decryptedCesiumDewif.toString());
  } on ChecksumException {
    print('Bad secret code');
  } catch (e) {
    print(e);
  }

  // Rebuild Cesium wallet, but this time from seed
  var reCesiumWallet = CesiumWallet.fromSeed(decryptedCesiumDewif!);
  print('Pubkey: ' + reCesiumWallet.pubkey);
}
