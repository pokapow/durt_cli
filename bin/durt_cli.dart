import 'package:args/args.dart';
import 'package:durt/durt.dart';
import 'dart:io';
import 'package:fast_base58/fast_base58.dart';

Future<void> main(List<String> arguments) async {
  final balanceCommand = ArgParser();
  final payCommand = ArgParser();
  final lockCommand = ArgParser();
  final unlockCommand = ArgParser();
  final pubkeyCommand = ArgParser();

  var parser = ArgParser()
    ..addFlag('help',
        negatable: false, abbr: 'h', help: 'Display the Durt help.')
    ..addCommand('balance', balanceCommand)
    ..addCommand('id')
        .addOption('pubkey', abbr: 'p', help: 'Display user of given pubkey')
    ..addCommand('generate').addOption('lang',
        abbr: 'l', help: 'Choose a mnemonic language (default is english)')
    ..addCommand('pubkey', pubkeyCommand)
    ..addCommand('pay', payCommand)
    ..addCommand('lock', lockCommand)
    ..addCommand('unlock', unlockCommand);

  balanceCommand
    ..addOption('pubkey', abbr: 'p', help: 'Display user of given pubkey')
    ..addFlag('ud', abbr: 'u', help: 'Return balance in UD reference.');

  payCommand
    ..addFlag('useMempool', abbr: 'e')
    ..addFlag('ud', abbr: 'u')
    ..addOption('recipient', abbr: 'r')
    ..addOption('dewif', abbr: 'd', help: 'DEWIF base64 format.')
    ..addOption('password', abbr: 'p')
    ..addOption('lang', abbr: 'l')
    ..addOption('mnemonic', abbr: 'm')
    ..addOption('cesiumid', abbr: 'i')
    ..addOption('cesiumpwd', abbr: 'w')
    ..addOption('derivation', abbr: 'n')
    ..addOption('amount', abbr: 'a')
    ..addOption('comment', abbr: 'c');

  lockCommand
    ..addOption('mnemonic', abbr: 'm')
    ..addOption('cesiumId', abbr: 'i')
    ..addOption('cesiumPassword', abbr: 'a')
    ..addOption('password', abbr: 'p')
    ..addOption('lang', abbr: 'l');

  unlockCommand
    ..addOption('dewif', abbr: 'd')
    ..addOption('password', abbr: 'p')
    ..addOption('lang', abbr: 'l')
    ..addFlag('cesium', abbr: 'c');

  pubkeyCommand
    ..addOption('mnemonic', abbr: 'm')
    ..addOption('derivation', abbr: 'n');

  ArgResults argResults;
  try {
    argResults = parser.parse(arguments);
  } catch (e) {
    print('Unknown option');
    exit(1);
  }

  if (argResults.wasParsed('help')) {
    print('''This is a command line tool which implement durt library.
${parser.usage}
''');
    parser.commands.forEach((cmdName, cmdOptions) {
      print(cmdName);
      cmdOptions.usage.split('\n').forEach((element) {
        print('   $element');
      });
      print('');
    });
    exit(0);
  } else if (argResults.command?.name == null) {
    print('Please enter a valid command.');
    exit(1);
  }
  var node = Gva(node: 'https://g1v1.p2p.legal/gva');
  // var node = Gva(node: 'https://g1.librelois.fr/gva');

  final command = argResults.command!;

  switch (command.name) {
    case 'balance':
      {
        String? pubkey = argResults.command?['pubkey'];
        if (pubkey == null) {
          print('Please enter a pubkey.');
          exit(1);
        } else {
          print(await node.balance(pubkey, ud: command.wasParsed('ud')));
        }
      }
      break;
    case 'id':
      {
        String? pubkey = argResults.command?['pubkey'];
        if (pubkey == null) {
          print('Please enter a pubkey.');
          exit(1);
        } else {
          print(await node.getUsername(pubkey));
        }
      }
      break;
    case 'pay':
      {
        String? recipient = argResults.command?['recipient'];
        String? dewif = argResults.command?['dewif'];
        String? password = argResults.command?['password'];
        String lang = argResults.command?['lang'] ?? 'english';
        String? mnemonic = argResults.command?['mnemonic'];
        String? cesiumid = argResults.command?['cesiumid'];
        String? cesiumpwd = argResults.command?['cesiumpwd'];
        int derivation = int.parse(argResults.command?['derivation'] ?? '-1');
        String comment = argResults.command?['comment'] ?? '';
        bool useMempool = argResults.command?['useMempool'] ?? false;
        bool isUd = argResults.command?['ud'];
        double amount = double.parse(argResults.command?['amount'] ?? '0');
        if (recipient == null ||
            (((dewif == null && password == null) && mnemonic == null ||
                    derivation == -1) &&
                (cesiumid == null && cesiumpwd == null)) ||
            amount == 0) {
          print(
              'Please enter a recipient, the amount of the transaction, the dewif base64 string and it password, and the derivation number');
          exit(1);
        } else {
          final result = await node.pay(
              recipient: recipient,
              dewif: dewif,
              password: password,
              mnemonic: mnemonic,
              lang: lang,
              cesiumId: cesiumid,
              cesiumPwd: cesiumpwd,
              amount: amount,
              comment: comment,
              derivation: derivation,
              ud: isUd,
              useMempool: useMempool);
          print(result);
        }
      }
      break;
    case 'lock':
      {
        String? mnemonic = argResults.command?['mnemonic'];
        String? cesiumId = argResults.command?['cesiumId'];
        String? cesiumPassword = argResults.command?['cesiumPassword'];
        String? password = argResults.command?['password'];
        String lang = argResults.command?['lang'] ?? 'french';
        if (password == null) {
          print('Please give me a password to encrypt DEWIF');
          exit(1);
        } else if (mnemonic != null) {
          final dewifWallet =
              await Dewif().generateDewif(mnemonic, password, lang: lang);
          print(dewifWallet.dewif);
        } else if (cesiumId != null && cesiumPassword != null) {
          final cesiumWallet = CesiumWallet(cesiumId, cesiumPassword);
          final dewifWallet =
              await Dewif().generateCesiumDewif(cesiumWallet.seed, password);
          print(dewifWallet.dewif);
        } else {
          print(
              'Please give me a mnemonic or a cesium ID + cesium password, with DEWIF password');
          exit(1);
        }
      }
      break;
    case 'unlock':
      {
        String? dewif = argResults.command?['dewif'];
        String? password = argResults.command?['password'];
        bool isCesium = argResults.command?['cesium'];
        if (dewif == null || password == null) {
          print('Please give me a dewif with it password');
          exit(1);
        } else if (isCesium) {
          try {
            print(Base58Encode(Dewif().cesiumSeedFromDewif(dewif, password)));
          } on ChecksumException {
            print('Bad secret code');
          } catch (e) {
            print(e);
          }
        } else {
          try {
            print(Dewif().mnemonicFromDewif(dewif, password, lang: 'french'));
          } on ChecksumException {
            print('Bad secret code');
          } catch (e) {
            print(e);
          }
        }
      }
      break;
    case 'generate':
      {
        String lang = argResults.command?['lang'] ?? 'english';
        print(generateMnemonic(lang: lang));
      }
      break;
    case 'pubkey':
      {
        int derivation = int.parse(argResults.command?['derivation'] ?? '0');
        String? mnemonic = argResults.command?['mnemonic'];
        if (mnemonic == null) {
          print('Please enter a mnemonic to derive.');
          exit(1);
        } else {
          final wallet = HdWallet.fromMnemonic(mnemonic);
          print(wallet.getPubkey(derivation));
        }
      }
      break;

    default:
      {
        print('Unknown command');
      }
      break;
  }

  exit(0);
}
