# Command line tool which implement durt library.

```
dart run bin/durt_cli.dart --help
```

You should find Linux executable in [release section](https://git.duniter.org/pokapow/durt_cli/-/releases).

```
./durt --help
```

```
This is a command line tool which implement durt library.
-h, --help    Display the Durt help.

balance
   -p, --pubkey     Display user of given pubkey
   -u, --[no-]ud    Return balance in UD reference.

id
   -p, --pubkey    Display user of given pubkey

generate
   -l, --lang    Choose a mnemonic language (default is english)

pubkey
   -m, --mnemonic      
   -n, --derivation    

pay
   -e, --[no-]useMempool    
   -u, --[no-]ud            
   -r, --recipient          
   -d, --dewif              DEWIF base64 format.
   -p, --password           
   -l, --lang               
   -m, --mnemonic           
   -i, --cesiumid           
   -w, --cesiumpwd          
   -n, --derivation         
   -a, --amount             
   -c, --comment            

lock
   -m, --mnemonic          
   -i, --cesiumId          
   -a, --cesiumPassword    
   -p, --password          
   -l, --lang              

unlock
   -d, --dewif          
   -p, --password       
   -l, --lang           
   -c, --[no-]cesium 
```